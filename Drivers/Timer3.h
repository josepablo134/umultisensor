#ifndef TIMER3CTL_H_
#define TIMER3CTL_H_
	#include <stdint.h>
	#define	TIMER3_HIGH_PRIORITY	1
	#define	TIMER3_LOW_PRIORITY		0
	extern void Timer3_init(void);
	extern void Timer3_enable(uint8_t);
	extern void Timer3_disable(void);
	extern void Timer3_start(void);
	extern void Timer3_stop(void);
	inline void Timer3_reload(uint16_t);
	inline uint16_t Timer3_capture(void);
	inline void Timer3_ISR(void);
#endif
