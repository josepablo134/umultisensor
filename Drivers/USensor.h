#ifndef USENSOR_H_
#define USENSOR_H_
	#include <stdint.h>
	#define		USensor_BIT0	0x00
	#define		USensor_BIT1	0x01
	#define		USensor_BIT2	0x02
	#define		USensor_BIT3	0x03
	#define		USensor_BIT4	0x04
	#define		USensor_BIT5	0x05
	#define		USensor_BIT6	0x06
	#define		USensor_BIT7	0x07
	#define		USensor_ALL		0x08
	typedef struct USensorData{
		uint16_t	time;
		int16_t	mm;
	}USensorData;
	extern void USensor_init(void);
	extern void USensor_measure(uint8_t);
	extern void USensor_read(USensorData*,uint8_t);
	inline void USensor_ISR(void);
#endif
