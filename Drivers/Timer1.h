#ifndef TIMER1CTL_H_
#define TIMER1CTL_H_
	#include <stdint.h>
	#define	TIMER1_HIGH_PRIORITY	1
	#define	TIMER1_LOW_PRIORITY		0
	extern void Timer1_init(void);
	extern void Timer1_enable(uint8_t);
	extern void Timer1_disable(void);
	extern void Timer1_start(void);
	extern void Timer1_stop(void);
	inline void Timer1_reload(uint16_t);
	inline uint16_t Timer1_capture(void);
	inline void Timer1_ISR(void);
#endif
