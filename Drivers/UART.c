#include "UART.h"
#include "../picconfig.h"

volatile uint8_t status;
volatile unsigned char *pTx;
volatile unsigned char *pRx;
volatile unsigned int iTx;
volatile unsigned int iRx;

    /// Inicializar el puerto serial
void UART_init(){
    status = 0x00;
    pTx = pRx = 0x00;
    iTx = iRx = 0x00;
    return;
}
void UART_open(unsigned long baud, uint8_t priority ){
    unsigned short buffer;
    //Configurar pines
    //TX(C6) - RX(C7)
    PORTC = 0;LATC  = 0;TRISC = 0xC0;
    
    /// Receive Active(0) - BRG16(1) - WUE(1)
    BAUDCON = 0b00001010;
    /// BRGH(0) - TXEN(0) - SYNC(0) : Modo Asincrono
    TXSTA = 0b00000000;
    /// SPEN(0) - CREN(0)           : Modo Asincrono
    RCSTA = 0b00000000;
    
    /// Generador de baudrate automatico
    // FOSC/[64(n+1)] = Baud
    //=>    [(FOSC/Baud)/64]-1;
    //SPBRGH = 0x00;
    if( !TXSTAbits.SYNC ){
        if( !BAUDCONbits.BRG16 ){
            if( TXSTAbits.BRGH ){
                SPBRG  = (unsigned char)((_XTAL_FREQ/baud/16)-1);
            }else{
                SPBRG  = (unsigned char)((_XTAL_FREQ/baud/64)-1);                
            }
        }else{
            if( TXSTAbits.BRGH ){
                buffer = (_XTAL_FREQ/baud/4)-1;
                SPBRG  = buffer & 0xFF;
                SPBRGH = buffer >> 8;
            }else{
                buffer = (_XTAL_FREQ/baud/16)-1; 
                SPBRG  = buffer & 0xFF;
                SPBRGH = buffer >> 8;
            }
        }
    }else{
        if( !BAUDCONbits.BRG16 ){
                SPBRG  = (unsigned char)((_XTAL_FREQ/baud/4)-1);
        }else{
                buffer = ((_XTAL_FREQ/baud/4)-1);
                SPBRG  = buffer & 0xFF;
                SPBRGH = buffer >> 8;
        }
    }
    
    ///Activar el puerto UART
    IPR1bits.TX1IP = priority & 0x01;//Sin interrupcion, pero registrada como high
    IPR1bits.RC1IP = priority & 0x01;//Sin interrupcion, pero registrada como high
    TXSTAbits.TXEN = 1;//Tx Enable
    RCSTAbits.CREN = 1;//Rx Enable
    RCSTAbits.SPEN = 1;//Serial Port Enable
}
    /// Comenzar una transmision
uint8_t UART_Transmit(void* buffer, unsigned int size){
    if( buffer && size ){ // Si la direccion o el tamanio son validos
        if( pTx ){/// Si no hay alguna transmision pendiente
            if( status & TX_BUSY ){
                return status;
            }else{
                goto SET_PTR;
            }
        }else{
            goto SET_PTR;
        }
    }else{
        return 0xFF;
    }
SET_PTR:
    pTx = buffer;
    iTx = size;
    status |= TX_BUSY;
    PIE1bits.TX1IE = 1;     //Activar sistema de interrupciones
    return 0;
}
void UART_CancelTransmit(void){
    PIE1bits.TX1IE = 0;     //Desactivar sistema de interrupciones
    status = (status&RX_MASK) | TX_CANCEL;
    return;
}
    /// Comenzar una recepcion
uint8_t UART_Receive(void* buffer, unsigned int size){
    if( buffer || size ){ // Si la direccion o el tamanio son validos
        if( pRx ){/// Si no hay alguna transmision pendiente
            if( status & RX_BUSY ){
                return status;
            }else{
                goto SET_PTR;
            }
        }else{
            goto SET_PTR;
        }
    }else{
        return 0xFF;
    }
SET_PTR:
    pRx = buffer;
    iRx = size;
    status |= RX_BUSY;
    PIE1bits.RC1IE = 1;     //Activar sistema de interrupciones
    return 0;
}
void UART_CancelReceive(void){
    PIE1bits.RC1IE = 0;     //Desactivar sistema de interrupciones
    status = (status&TX_MASK) | RX_CANCEL;
    return;
}
    /// Solicitar el estado de la transferencia
uint8_t UART_Status(void){
    return status;
}

void inline UART_ISR(void){
	if(PIR1bits.RC1IF){                        
	 if( status & RX_BUSY ){
		*pRx = RCREG;                           
		*pRx++;                                 
		iRx--;                                  
		if( !iRx ){                             
			status &= TX_MASK;                  
			PIE1bits.RCIE = 0;                  
		}                                       
	 }
	}                                          
	if(PIR1bits.TXIF){                         
	 if( status & TX_BUSY ){
		iTx--;                                  
		if( iTx ){                              
			TXREG = *pTx;                       
			*pTx++;                             
		}else{                                  
			status &= RX_MASK;                  
			PIE1bits.TX1IE = 0;                 
		}                                       
	 }
	}                                           
}
