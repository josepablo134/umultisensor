#ifndef _UART_H
#define	_UART_H
    #include <stdint.h>
	
    #define TX_MASK             0x0F
    #define TX_BUSY             0x01
    #define TX_CANCEL           0x02
    #define RX_MASK             0xF0
    #define RX_BUSY             0x10
    #define RX_CANCEL           0x20
    #define DEFAULT_BAUD    	115200
	#define	UART_HIGH_PRIORITY	1
	#define	UART_LOW_PRIORITY	0


    extern volatile unsigned char          *pTx,*pRx;
    extern volatile unsigned int            iTx,iRx;
    extern volatile uint8_t                 status;
    
    /// Inicializar el puerto serial
    extern void UART_init(void);
    /// Inicializar el puerto serial
    extern void UART_open(unsigned long,uint8_t);
    /// Comenzar una transmision
    extern uint8_t UART_Transmit(void* , unsigned int);
    extern void UART_CancelTransmit(void);
    /// Comenzar una recepcion
    extern uint8_t UART_Receive(void* , unsigned int);
    extern void UART_CancelReceive(void);
    /// Solicitar el estado de la transferencia
    extern uint8_t UART_Status(void);
	void inline UART_ISR(void);    
#endif
