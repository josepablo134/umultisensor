#include "USensor.h"
#include "Timer3.h"
#include "../picconfig.h"

#define	USensorFactor	(uint16_t)16

volatile USensorData		measure[8];
volatile uint8_t            USensorMEASFlag;

void USensor_init(void){
	register uint8_t	counter;
	PORTB = LATB = 0;
    TRISB = 0x01;
    ADCON1 = 0x0F;
    CMCON = 0x07;

	Timer3_init();
	Timer3_reload( 0 );
	for( counter=0;counter<8;counter++ ){
		measure[counter].time = 0xFF;
		measure[counter].mm = 0xFF;
	}
}

void USensor_measure(uint8_t mode){
    volatile static uint16_t  tempTime=0;
	///	Prepare Timer3
	Timer3_stop();
	Timer3_reload( 0 );
    Timer3_enable( TIMER3_HIGH_PRIORITY );
	///	Prepare monitor flags
	if( mode>8 ){ return; }
    USensorMEASFlag = 1;
	////	Send Trigger signal
	asm("BSF	LATB,1");
	__delay_us( 10 );
	asm("BCF	LATB,1");
	Timer3_start();
    __delay_us( 700 );
    while( (!PORTBbits.RB0) & USensorMEASFlag ){}
    tempTime = Timer3_capture();
    while( PORTBbits.RB0 & USensorMEASFlag ){}
    Timer3_stop();
    if(USensorMEASFlag){
        tempTime = Timer3_capture() - tempTime;
        measure[mode].time = tempTime;
        measure[mode].mm = (int16_t)(tempTime / USensorFactor);    
    }else{
        measure[mode].time = 0xFFFF;
        measure[mode].mm = -1;
    }
}

void USensor_read(USensorData* buf,uint8_t index){
	if( !buf ){return;}
	if( index > 8 ){return;}
	*buf = measure[index];
	return;
}

inline void USensor_ISR(void){
	if(PIR2bits.TMR3IF){
        /// USensor Timeout reach
		///	Stop and clear interrupt
		T3CON &= 0xFE;
		PIR2bits.TMR3IF = 0;
		USensorMEASFlag = 0;
	}
}
