#include "Timer3.h"
#include "../picconfig.h"

void Timer3_init(void){
	///	(FOSC/4)/4 @48Mhz -> 3Mhz
	T3CONbits.RD16 = 0b01;
    T3CONbits.T3CKPS = 0b10;
}
void Timer3_enable(uint8_t priority){
	PIR2bits.TMR3IF = 0;
	IPR2bits.TMR3IP = priority & 0x01;
	PIE2bits.TMR3IE = 1;
}
void Timer3_disable(void){
	PIE2bits.TMR3IE = 0;
}
void Timer3_start(void){
	T3CON |= 0x01;
}
void Timer3_stop(void){
	T3CON &= 0xFE;
	PIR2bits.TMR3IF = 0;
}
inline void Timer3_reload(uint16_t value){
	TMR3H = (uint8_t)((value>>8) & 0xFF);
	TMR3L = (uint8_t)((value) & 0xFF);
}
inline uint16_t Timer3_capture(void){
	volatile static uint16_t	time;
    vPortENTER_CRITICAL();
        time = TMR3;
		//time = (uint16_t)TMR3L;
        //time |= ((((uint16_t)TMR3H)<<8) & 0xFF00);
    vPortEXIT_CRITICAL();
	return time;
}
