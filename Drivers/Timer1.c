#include "Timer1.h"
#include "../picconfig.h"

const uint32_t	T1FREQ=_XTAL_FREQ/4;

void Timer1_init(void){
	///	Internal OSC FOSC/4
	T1CON =0x80;
}
void Timer1_enable(uint8_t priority){
	PIR1bits.TMR1IF = 0;
	IPR1bits.TMR1IP = priority & 0x01;
	PIE1bits.TMR1IE = 1;
}
void Timer1_disable(void){
	PIE1bits.TMR1IE = 0;
}
void Timer1_start( void ){
	T1CON |= 0x01;
}
void Timer1_stop(void){
	T1CON &= 0xFE;
}
inline void Timer1_reload(uint16_t value){
	TMR1 = value;
}
inline uint16_t Timer1_capture(void){
	return TMR1;
}
