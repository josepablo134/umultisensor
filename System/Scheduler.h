#ifndef SCHEDUELR_H_
#define SCHEDUELR_H_
	#include <stdint.h>
	#include "List.h"
	#define SYSTICK_PRIOR	TIMER1_LOW_PRIORITY 
    #define xTimeWAIT_FOREVER       0xFFFF

	typedef enum SchedulerStates{
		SchedulerDone=0,
		SchedulerWaiting,
		SchedulerReady,
		SchedulerRunning,
	}SchedulerStates;
	typedef enum TaskState{
		TaskDone=0,
		TaskReady,
		TaskRunning,
		TaskWaiting,
		TaskBlock,
	}TaskState;
	typedef void (*Task)(void);
	typedef struct SchedulerTask{
		Task				fxn;
		uint8_t				state;
		TickType_t			delay;
		TickType_t			stamp;
		ListItem_t			node;
	}SchedulerTask;
	typedef struct SchedulerTask* TaskHandle;
	
	extern void Scheduler_init(void);
	extern TaskHandle Scheduler_createTask( Task , TickType_t );
	extern void Scheduler_start(void);
	extern void Scheduler_stop(void);
	extern void Scheduler_wait(UBaseType_t);
	extern void Scheduler_suspend( TaskHandle );
	extern void Scheduler_resume( TaskHandle );
	inline void Scheduler_suspendISR( TaskHandle );
	inline void Scheduler_resumeISR( TaskHandle );
	inline void Scheduler_SysTickIncrement(void);
#endif
