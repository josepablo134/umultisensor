#include "../Drivers/Timer1.h"
#include "Scheduler.h"
#include "List.h"
#include "mem.h"

inline UBaseType_t uxListInsertEndISR( List_t*, ListItem_t*);
inline UBaseType_t uxListRemoveISR( ListItem_t* );

#define	SYSTICK_FREQ		12000000
#define	SYSTICK_US			1000
#define SYSTICK_TIME        (65535-((uint16_t)(SYSTICK_FREQ/SYSTICK_US)))
#define SYSTICK_PRIOR		TIMER1_LOW_PRIORITY 

volatile List_t				tskReadyQueue;
volatile List_t				tskWaitingQueue;
volatile List_t				tskBlockedQueue;
volatile List_t				tskDoneQueue;
volatile TaskHandle			tskRunning=0x00;

static TickType_t 			tskSysTick=0;

volatile uint8_t	SchedulerState=SchedulerReady;

void Scheduler_sort(void){
	///	Que pasa cuando el tiempo de retardo sea igual a un desbordamiento
	///	Quiza si almaceno el systick y el delay, para luego procesar
	///	la diferencia en el ordenamiento
	return;
}

TaskHandle Scheduler_createTask( Task taskFxn , TickType_t timeout ){
	TaskHandle		temp;
/*
		Task				fxn;
		uint8_t				state;
		TickType_t			delay;
		ListItem_t			node;
*/
	temp = (TaskHandle) malloc( sizeof( SchedulerTask ) );
	if( !temp ){ return 0x00; }
	temp->fxn = taskFxn;
    vListInitialiseItem( &temp->node );
	temp->node.pvData = temp;
	if( timeout == xTimeWAIT_FOREVER ){
		temp->state = TaskBlock;
   		uxListInsertEnd( (List_Handle) &tskBlockedQueue , &temp->node );
	}else
	if( timeout ){
		temp->delay = timeout;
		temp->state = TaskWaiting;
   		uxListInsertEnd( (List_Handle) &tskWaitingQueue , &temp->node );
	}else{
		temp->state = TaskReady;
    	uxListInsertEnd( (List_Handle) &tskReadyQueue , &temp->node );
	}
	return temp;
}
void Scheduler_wait(UBaseType_t time){
	static TickType_t	actual;
	if( !tskRunning ){ return; }
	if( !time ){
		tskRunning->state = TaskReady;
		return;
	}
	vPortENTER_CRITICAL();
		actual = tskSysTick;
	vPortEXIT_CRITICAL();
	//tskRunning->delay = actual+time;
	tskRunning->delay = time;
	tskRunning->stamp = actual;
	tskRunning->state = TaskWaiting;
	///	Dispatcher should verify state value to queue at next list
}
void Scheduler_suspend( TaskHandle taskH ){
	if( !taskH ){return;}
	taskH->state = TaskBlock;
	///	If it is possible, queue this task
	if( tskRunning != taskH ){
    	uxListRemove( &taskH->node );
    	uxListInsertEnd( (List_Handle) &tskBlockedQueue , &taskH->node );
	}
	///	If not, let the distpacher do it for us
}
void Scheduler_resume( TaskHandle taskH ){
	if( !taskH ){return;}
	taskH->state = TaskReady;
	///	If it is possible, queue this task
	if( tskRunning != taskH ){
    	uxListRemove( &taskH->node );
    	uxListInsertEnd( (List_Handle) &tskReadyQueue , &taskH->node );
	}
	///	If not, let the dispatcher do it for us
}
inline void Scheduler_suspendISR( TaskHandle taskH ){
	if( !taskH ){return;}
	taskH->state = TaskBlock;
	///	If it is possible, queue this task
	if( tskRunning != taskH ){
		uxListRemoveISR( &taskH->node );
    	uxListInsertEndISR( (List_Handle) &tskBlockedQueue , &taskH->node );
	}
	///	If not, let the distpacher do it for us
}
inline void Scheduler_resumeISR( TaskHandle taskH ){
	static TaskHandle temp;
	if( !taskH ){return;}
	taskH->state = TaskReady;
	///	If it is possible, queue this task
	if( tskRunning != taskH ){
		uxListRemoveISR( &taskH->node );
    	uxListInsertEndISR( (List_Handle) &tskReadyQueue , &taskH->node );
	}
	///	If not, let the dispatcher do it for us
}

void Scheduler_init(void){
    vListInitialise( (List_Handle) &tskReadyQueue );
    vListInitialise( (List_Handle) &tskWaitingQueue );
    vListInitialise( (List_Handle) &tskBlockedQueue );
    vListInitialise( (List_Handle) &tskDoneQueue );
	Timer1_init();
    tskSysTick = 0;
}


void vTaskSwitchContext(void){
	if( tskReadyQueue.size ){
		/// Cargar al buffer la tarea
		tskRunning = (TaskHandle) (tskReadyQueue.pvRoot->pvData);
		/// Eliminar de la lista ready
		uxListRemove(&tskRunning->node);
		return;
	}
    /// No hay tarea disponible para ejecucion
    tskRunning = 0x00;
}
volatile static uint16_t __temp_wreg;    
void vTaskEXEC_TASK(void){
    /// Subir ptr de function a pila
    vPortENTER_CRITICAL();
        asm("PUSH");
        __temp_wreg = (uint16_t)(tskRunning->fxn);
        asm("MOVF   ___temp_wreg,W");
        asm("MOVWF  TOSL");
        asm("MOVF   ___temp_wreg+1,W");
        asm("MOVWF  TOSH");
        asm("CLRF  TOSU");
    vPortEXIT_CRITICAL();
    //tskRunning->function();
    /// Retornar a la funcion
    return;
}

inline void Scheduler_check(void){
    static ListItem_Handle	pNode;
    static ListItem_Handle	pNode_temp;
    static TaskHandle		ptskNode;
    static TickType_t		actual;
	static TickType_t		delta;
	vPortENTER_CRITICAL();
		actual = tskSysTick;
	vPortEXIT_CRITICAL();
    pNode       = (ListItem_Handle)tskWaitingQueue.pvRoot;
    pNode_temp  = (ListItem_Handle)tskWaitingQueue.pvRoot;
    while( pNode_temp ){
        /// Obtener el TaskHandle del nodo
        ptskNode = ((TaskHandle)pNode_temp->pvData);
        /// Apuntar a la siguiente tarea
        pNode = ((ListItem_Handle)pNode_temp->pvNext);
        ///Cuando la tarea alcanza el tiempo solicitado
        // Es removida de la lista de espera y agregada a
        //  la lista Ready
        //if( actual == ptskNode->delay ){
		///@josepablo:	delta is time from stamp to actual
		delta = actual - ptskNode->stamp;
        if( delta >= ptskNode->delay ){
            /// Quitar de la lista WAITING
            uxListRemove( pNode_temp );
            /// Agregar a la lista READY
            uxListInsertEnd(
                        (List_Handle)&(tskReadyQueue) ,
                        (ListItem_Handle)pNode_temp
                    );
        }
        /// Avanzamos en la lista
        pNode_temp = pNode;
    }
	SchedulerState = SchedulerRunning;
}

void Scheduler_start(void){
	static TaskHandle temp;
	Timer1_reload( SYSTICK_TIME );
	Timer1_enable( SYSTICK_PRIOR );
	Timer1_start();
	
	Scheduler_sort();
	
	///	Distpacher read from ready queue and execute
	while( SchedulerState ){
		if( SchedulerState == SchedulerReady ){
			Scheduler_check();
		}
		vTaskSwitchContext();
		///	There is something to execute?
		if( tskRunning ){
			tskRunning->state = TaskRunning;
        	vTaskEXEC_TASK();
		}else{ continue; }
		
		///	Task asked to sleep?
		if( tskRunning->state == TaskWaiting ){
            uxListInsertEnd( (List_Handle) &tskWaitingQueue,
                        	(ListItem_Handle) &tskRunning->node
							);
			Scheduler_sort();
		}else
		///	Task asked for wait an async event?
		if( tskRunning->state == TaskBlock ){
            uxListInsertEnd( (List_Handle) &tskBlockedQueue,
                        	(ListItem_Handle) &tskRunning->node
							);
		}else
		///	Task asked for Yield to another task?
		if( tskRunning->state == TaskReady ){
			uxListInsertEnd( (List_Handle) &tskReadyQueue,
                        	(ListItem_Handle) &tskRunning->node
							);
		}else
		///	Task is ready to finish?
		{
            tskRunning->state = TaskDone;
            uxListInsertEnd(
                        (List_Handle) &(tskDoneQueue) ,
                        (ListItem_Handle) &tskRunning->node
                    );
		}
	}
}
///	Stop SysTick and stop dispatcher
void Scheduler_stop(void){
	Timer1_stop();
	SchedulerState = SchedulerDone;
}

inline void Timer1_ISR(void){
	if( PIR1bits.TMR1IF ){
		Timer1_reload( SYSTICK_TIME );
		PIR1bits.TMR1IF = 0;
		tskSysTick++;
		SchedulerState = SchedulerReady;
	}
}

inline UBaseType_t uxListInsertEndISR( List_t* pxList, ListItem_t* pxNewListItem ){
    if( !pxList ){return 0x00;}
    if( !pxNewListItem ){return 0x00;}
    if( pxList->size ){
        /// El nuevo elemento apunta a NULO como su SIGUIENTE
        pxNewListItem->pvNext = 0x00;
        /// El nuevo elemento apunta al ultimo elemento como su PREVIO
        pxNewListItem->pvLast = pxList->pvLast;
        /// El ultimo elemento apunta al nuevo elemento como su SIGUIENTE
        ((ListItem_Handle)(pxList->pvLast))->pvNext = pxNewListItem;
        /// La lista asigna la calidad de ULTIMO al nuevo elemento
        pxList->pvLast = (void*) pxNewListItem;
        /// La lista crece un elemento
        pxList->size += 1;
    }else{
        /// CUANDO LA LISTA ESTA VACIA
        /// EL ULTIMO ELEMENTO ES EL PRIMERO
        /// Y LOS EXTREMOS APUNTAN NULO
        /// YA QUE NO HAY NI SIGUIENTE NI ANTERIOR
        pxList->pvRoot = (void*)pxNewListItem;
        pxList->pvLast = (void*)pxNewListItem;
        pxNewListItem->pvLast = pxNewListItem->pvNext = 0x00;
        pxList->size += 1;
    }
    /// En cualquiera de los casos, el nodo apunta a la lista
    pxNewListItem->pvList = (void*)pxList;

    return pxList->size;
}

inline UBaseType_t uxListRemoveISR( ListItem_t* pxItemToRemove ){
    register UBaseType_t    left;
    /// EL nodo no existe
    if( !pxItemToRemove ){ return 0x00; }
    /// El nodo no ha sido asignado a alguna lista 
    if( !pxItemToRemove->pvList ){ return 0x00; }
    
    /// Si el nodo es el unico en la lista
    if( !pxItemToRemove->pvLast && !pxItemToRemove->pvNext ){
        /// Desligamos la lista del nodo
        ((List_Handle)pxItemToRemove->pvList)->pvRoot = 0x00;
        ((List_Handle)pxItemToRemove->pvList)->pvLast = 0x00;
    }
    /// Si el nodo es Root
    else if( !pxItemToRemove->pvLast ){
        /// EL nodo SIGUIENTE al nodo ROOT es ahora el nodo ROOT
        ((List_Handle)pxItemToRemove->pvList)->pvRoot = pxItemToRemove->pvNext;
        /// Este nuevo nodo ROOT apunta a NULO como su PREVIO
        ((ListItem_Handle)pxItemToRemove->pvNext)->pvLast = 0x00;
    }
    /// Si el nodo es LAST
    else if( !pxItemToRemove->pvNext  ){
        /// EL nodo ANTERIOR al nodo LAST es ahora el nodo LAST
        ((List_Handle)pxItemToRemove->pvList)->pvLast = pxItemToRemove->pvLast;
        /// Este nuevo nodo LAST apunta a NULO como su SIGUIENTE
        ((ListItem_Handle)pxItemToRemove->pvLast)->pvNext = 0x00;
    }
    /// Si el nodo es un nodo INTERMEDIO
    else{
        /// El nodo PREVIO apunta al nodo SIGUIENTE
        ((ListItem_Handle)pxItemToRemove->pvLast)->pvNext = (void*)((ListItem_Handle)pxItemToRemove->pvNext);
        /// El nodo SIGUIENTE apunta al nodo PREVIO
        ((ListItem_Handle)pxItemToRemove->pvNext)->pvLast = (void*)((ListItem_Handle)pxItemToRemove->pvLast);
    }
    /// En cualquiera de los casos, La lista decrementa sus nodos en uno
    ((List_Handle)pxItemToRemove->pvList)->size -= 1;
    left = ((List_Handle)pxItemToRemove->pvList)->size;
    /// Y desligamos al nodo de cualquier contexto
    pxItemToRemove->pvLast = 0x00;
    pxItemToRemove->pvNext = 0x00;
    pxItemToRemove->pvList = 0x00;
    
    return left;
}
