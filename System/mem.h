/**
* @date     07/11/2018
* @version  1.0.0
* @autor    Josepablo Cruz Baas
* @description
*   Mem man es una API de memoria dinamica,
*   provee las interfaces para la reservacion y
*   liberacion de memoria dinamica estilo POSIX.
*/
#ifndef _MEM_MAN_H_
#define _MEM_MAN_H_
	#include <stdint.h>
    /**
    * The malloc() function allocates size bytes and returns a pointer to
    *   the allocated memory.  The memory is not initialized.  If size is 0,
    *   then malloc() returns either NULL, or a unique pointer value that can
    *   later be successfully passed to free().
    */
    extern void*   malloc( uint32_t );
    extern void    free( void* );
#endif
