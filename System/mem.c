#include "mem.h"
#include "../picconfig.h"

volatile uint8_t  HEAP[ MEM_MAN_MAX_HEAP_SIZE ];
uint32_t HEAP_counter=0;

void*   malloc(uint32_t size){
    vPortENTER_CRITICAL();
    void*  pvTemp = 0x00;///Apuntar a nulo
    /// Si hay espacio suficiente y (size != 0)
    if( (HEAP_counter+size) < MEM_MAN_MAX_HEAP_SIZE && size){
        pvTemp = (void*)(&HEAP[ HEAP_counter ]);
        HEAP_counter += size;
    }else{
        /// No hay espacio suficiente.
        HEAP_counter -= size;
    }
    vPortEXIT_CRITICAL();
    /// Puede ser tanto NULL como un apuntador asignado
    return pvTemp;
}

void free( void* pbuffer ){
    /// No existe la utilidad free!
    return;
}
