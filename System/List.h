/**
* @date     12/01/2019
* @version  1.0.1
* @autor    Josepablo Cruz Baas
* @description
*   List es una libreria de lista ligada,
*   provee las interfaces para la creacion
*   y manipulacion de listas ligadas y nodos.
*/
#ifndef _LIST_H_
#define	_LIST_H_
	#include "../picconfig.h"
    ///////////////////////////////////
    //  LIST TYPES
    ///////////////////////////////////
    typedef struct ListItem_t{
        struct ListItem_t*      pvLast;
        void*                   pvData;
        void*                   pvList;
        struct ListItem_t*      pvNext;
    }ListItem_t;
    typedef struct List_t{
        ListItem_t*             pvRoot;
        ListItem_t*             pvLast;
        uint16_t                size;
    }List_t;
    typedef List_t* List_Handle;
    typedef ListItem_t* ListItem_Handle;
	
    /*
     * Must be called before a list is used!  This initialises all the members
     * of the list structure and inserts the xListEnd item into the list as a
     * marker to the back of the list.
     *
     * @param pxList Pointer to the list being initialised.
     *
     */
    extern void vListInitialise(List_t* pxList );

    /*
     * Must be called before a list item is used.  This sets the list container to
     * null so the item does not think that it is already contained in a list.
     *
     * @param pxItem Pointer to the list item being initialised.
     *
     */
    extern void vListInitialiseItem(ListItem_t* pxItem );

    /*
     * Insert a list item at the beginning of a list.
     *
     * @param pxList The list into which the item is to be inserted.
     *
     * @param pxNewListItem The item that is to be placed in the list.
     *
     */
    extern UBaseType_t uxListInsert(List_t* pxList,ListItem_t* pxNewListItem );

    /*
     * Insert a list item at the end of a list.
     *
     * @param pxList The list into which the item is to be inserted.
     *
     * @param pxNewListItem The list item to be inserted into the list.
     *
     */
    extern UBaseType_t uxListInsertEnd( List_t* pxList,ListItem_t* pxNewListItem );

    /*
     * Remove an item from a list.
     *
     * @param pxItemToRemove The item to be removed.  The item will remove itself from
     * the list pointed to by it's pxContainer parameter.
     *
     * @return The number of items that remain in the list after the list item has
     * been removed.
     */
    extern UBaseType_t uxListRemove( ListItem_t* pxItemToRemove );

    /*
     * Reeplace an item from a list.
     *
     * @param pxItemToRemove The item to be removed.  The item will remove itself from
     * the list pointed to by it's pxContainer parameter.
     *
     * @param pxItemToInsert The item to be inserted.
     *
     * @return The number of items that remain in the list after the list item has
     * been removed.
     */
    extern UBaseType_t uxListReplace( ListItem_t* pxItemToRemove , ListItem_t* pxItemToInsert );

    /*
     * Insert an item in a list after an defined item.
     *
     * @param pxItem The reference item.
     *
     * @param pxItemToInsert The item to insert.
     *
     */
    extern UBaseType_t uxListInsertAfter( ListItem_t* pxItem, ListItem_t* pxItemToInsert );

    /*
     * Remove an item from a list.
     *
     * @param uxListRemove The item to be removed.  The item will remove itself from
     * the list pointed to by it's pxContainer parameter.
     *
     * @return The number of items that remain in the list after the list item has
     * been removed.
     */
    extern UBaseType_t uxListInsertBefore( ListItem_t* pxItem, ListItem_t* pxItemToInsert );
#endif
