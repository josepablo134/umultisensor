#pragma warning disable 520     //Function not used
#pragma warning disable 1498    //pointer could not set
#pragma warning disable 2053

#define ERROR_HANDLER(){while(1);}

#include "picconfig.h"
#include "Drivers/UART.h"
#include "Drivers/Timer1.h"
#include "Drivers/USensor.h"
#include "System/Scheduler.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

TaskHandle	Task0Handler;
void Task0(void);

TaskHandle	Task1Handler;
void Task1(void);

void main(void) {
	INTCONbits.GIE = 1;
	INTCONbits.GIEL = 1;
	RCONbits.IPEN = 1;
	
	UART_init();
	UART_open(115200 , UART_HIGH_PRIORITY );

	USensor_init();
	
	Scheduler_init();
	Task0Handler = Scheduler_createTask( Task0 , 0 );
	if(! Task0Handler ){
		ERROR_HANDLER();
	}
	Task1Handler = Scheduler_createTask( Task1 , xTimeWAIT_FOREVER );
	if(! Task1Handler ){
		ERROR_HANDLER();
	}

	vPortENABLE_INTERRUPTS();
	Scheduler_start();

	Task0();
	Task1();
}

static char msg[15];
void Task0(void){
	static USensorData	data;
	Scheduler_wait(500);
	USensor_measure( USensor_BIT0 );
	USensor_read( &data , USensor_BIT0 );
    snprintf( msg , sizeof(msg) , "%d mm\n" , data.mm );
    UART_Transmit( msg , strlen(msg)+1 );
}

void Task1(void){
	Scheduler_suspend( Task1Handler );
	asm("BTG	LATA,4");
}

void __interrupt(high_priority) high_isr(void){
	USensor_ISR();
	UART_ISR();
}

void __interrupt(low_priority) low_isr(void){
	Timer1_ISR();
}

